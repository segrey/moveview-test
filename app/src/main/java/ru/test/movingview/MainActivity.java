package ru.test.movingview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.lang.ref.WeakReference;

/**
 * @author segrey
 */

public class MainActivity extends Activity {
    
    private static final String STATE_CURRENT_POINT = "state_current_point";
    private static final String STATE_CURRENT_ROTATION = "state_current_rotation";
    
    private static final long FULL_SCREEN_DURATION = 2 * DateUtils.SECOND_IN_MILLIS;
    
    private View mContainer;
    private View mView;
    
    private PointF mPoint;
    private Float mRotation;
    
    private int fieldWidth, fieldHeight;
    
    private Animator currentAnimator;
    private MovingViewAnimatorListener currentListener;
    private boolean isAnimating;
    
    @Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);
        
        mContainer = findViewById(R.id.fieldView);
        mContainer.setOnTouchListener(new ContainerTouchListener(this));
        mView = mContainer.findViewById(R.id.movingView);
        
        if ((mPoint == null || mRotation == null) && savedInstanceState != null) {
            mPoint = savedInstanceState.getParcelable(STATE_CURRENT_POINT);
            if (savedInstanceState.containsKey(STATE_CURRENT_ROTATION)) {
                mRotation = savedInstanceState.getFloat(STATE_CURRENT_ROTATION);
            }
        }
        
    }
    
    @Override protected void onResume() {
        super.onResume();
        mContainer.post(new Runnable() {
            @Override public void run() {
                onFirstLayout();
            }
        });
    }
    
    @Override protected void onPause() {
        super.onPause();
        if (currentAnimator != null) {
            currentAnimator.cancel();
        }
        currentAnimator = null;
        currentListener = null;
    }
    
    @Override protected void onSaveInstanceState(final Bundle outState) {
        mPoint = new PointF(
            (mView.getTranslationX() + mView.getMeasuredWidth() / 2) / fieldWidth,
            (mView.getTranslationY() + mView.getMeasuredHeight() / 2) / fieldHeight
        );
        mRotation = mView.getRotation();
        outState.putParcelable(STATE_CURRENT_POINT, mPoint);
        outState.putFloat(STATE_CURRENT_ROTATION, mRotation);
        super.onSaveInstanceState(outState);
    }
    
    private void onFirstLayout() {
        fieldWidth = mContainer.getMeasuredWidth();
        fieldHeight = mContainer.getMeasuredHeight();
        if (mPoint == null) {
            mPoint = new PointF(0.5f, 0.5f);
        }
        if (mRotation == null) {
            mRotation = -90.0f;
        }
        mView.setRotation(mRotation);
        mView.setTranslationX(fieldWidth * mPoint.x - mView.getMeasuredWidth() / 2);
        mView.setTranslationY(fieldHeight * mPoint.y - mView.getMeasuredHeight() / 2);
    }
    
    private void moveView(float toX, float toY) {
        if (currentAnimator != null) {
            currentAnimator.cancel();
        }
        if (currentListener == null) {
            currentListener = new MovingViewAnimatorListener(this);
        }
        final float currentX = mView.getTranslationX();
        final float currentY = mView.getTranslationY();
        float currentRotation = mView.getRotation();
        if (currentRotation < 0) {
            currentRotation += 360;
            mView.setRotation(currentRotation);
        }
        if (currentX != toX || currentY != toY) {
            final float targetX = toX - mView.getMeasuredWidth() / 2;
            final float targetY = toY - mView.getMeasuredHeight() / 2;
            
            float targetRotation = -1 * getRotationAngle(currentX, currentY, targetX, targetY);
            if (targetRotation < 0) {
                targetRotation += 360;
            }
            if (Math.abs(currentRotation - targetRotation) > 90) {
                targetRotation += 180 * (targetRotation > 180 ? -1 : 1);
            }
            
            final PropertyValuesHolder tX = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, targetX);
            final PropertyValuesHolder tY = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, targetY);
            final AnimatorSet animatorSet = new AnimatorSet();
            animatorSet
                .play(ObjectAnimator.ofPropertyValuesHolder(mView, tX, tY))
                .with(ObjectAnimator.ofFloat(mView, View.ROTATION, targetRotation));
            
            currentAnimator = animatorSet;
            currentAnimator.setDuration(Math.round(
                Math.max(Math.abs(currentX - toX) / fieldWidth, Math.abs(currentY - toY) / fieldHeight) * FULL_SCREEN_DURATION
            ));
            isAnimating = true;
            currentAnimator.addListener(currentListener);
            currentAnimator.start();
        }
    }
    
    private float getRotationAngle(final float sourceX, final float sourceY, final float targetX, final float targetY) {
        float angle = (float) Math.toDegrees(Math.atan2(sourceY - targetY, targetX - sourceX));
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }
    
    private void onAnimationFinished() {
        isAnimating = false;
    }
    
    
    private static class ContainerTouchListener implements View.OnTouchListener {
        private final GestureDetector mDetector;
        
        ContainerTouchListener(final MainActivity activity) {
            mDetector = new GestureDetector(activity, new TapListener(activity));
        }
        
        @Override public boolean onTouch(final View v, final MotionEvent event) {
            return mDetector.onTouchEvent(event);
        }
    }
    
    
    private static class TapListener extends GestureDetector.SimpleOnGestureListener {
        private final WeakReference<MainActivity> activityRef;
        
        TapListener(final MainActivity activity) {
            activityRef = new WeakReference<>(activity);
        }
        
        @Override public boolean onDown(final MotionEvent e) {
            return true;
        }
        
        @Override public boolean onSingleTapConfirmed(final MotionEvent e) {
            final MainActivity activity = activityRef.get();
            if (activity != null && !activity.isAnimating && e != null) {
                activity.moveView(e.getX(), e.getY());
            }
            return true;
        }
    }
    
    
    private static class MovingViewAnimatorListener extends AnimatorListenerAdapter {
        private final WeakReference<MainActivity> activityRef;
        
        MovingViewAnimatorListener(final MainActivity activity) {
            activityRef = new WeakReference<>(activity);
        }
        
        @Override public void onAnimationCancel(final Animator animation) {
            onAnimationFinished();
        }
        
        @Override public void onAnimationEnd(final Animator animation) {
            onAnimationFinished();
        }
    
        private void onAnimationFinished() {
            final MainActivity activity = activityRef.get();
            if (activity != null) {
                activity.onAnimationFinished();
            }
        }
    }
    
}
